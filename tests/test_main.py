#!/usr/bin/env python
# Tests for fxns in main.py

import math

from ci_test.main import rss, square

def test_rss():
    assert( abs(rss([1,1,1]) - math.sqrt(3)) < 1e-5 )

def test_square():
    assert(square(2) == 4)