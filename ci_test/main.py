#!/usr/bin/env python
# Test for GitLab CI and Pytest framework

import math

def rss(args):
    """
    Finds the root sum square of the input args list.
    """
    return math.sqrt(sum([x**2 for x in args]))

def square(arg):
    """
    Squares the input.
    """
    assert(type(arg) is float or type(arg) is int)
    return arg**2